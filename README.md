Based on Golan Parashi's BitmapEditor for the [Nand2Tetris](www.nand2tetris.org) course

# Changes
* Publicly available at https://arieljannai.gitlab.io/Nand2TetrisBitmapEditor/
* New look
* Added `negative` bitmap board operation.
* Added `shift` operations buttons
* Added a `copy to clipboard` button.
* Added the ability to `color pixels with mouse dragging`.


# Origianl work
Can be found [here][1] or under the Nand2Tetris course project files, under project 09.

[1]: https://gitlab.com/arieljannai/Nand2TetrisBitmapEditor/blob/298d09aaff6c1f3987eea5be23521360ee4cb124/BitmapEditor.html