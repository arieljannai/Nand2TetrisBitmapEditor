// from https://www.sitepoint.com/offline-web-apps-service-workers-pouchdb/
var CACHE_NAME = 'nand2tetris-bitmap-editor-0.0.1';

var resourcesToCache = [
  'js/BitmapEditor.js',
  'css/BitmapEditor.css',
  'css/emoji.css',
  'js/register-service-worker.js'
];

self.addEventListener('install', function(event) {
  event.waitUntil(
    // open the app browser cache
    caches.open(CACHE_NAME)
      .then(function(cache) {
        // add all app assets to the cache
        return cache.addAll(resourcesToCache);
      })
  );
});

self.addEventListener('fetch', function(event) {
  event.respondWith(
    // try to find corresponding response in the cache
    caches.match(event.request)
      .then(function(response) {
        if (response) {
          // cache hit: return cached result
          // console.log('response: ', response);
          return response;
        }

        // not found: fetch resource from the server
        // console.log('not found: ', event.request);
        return fetch(event.request);
      })
  );
});