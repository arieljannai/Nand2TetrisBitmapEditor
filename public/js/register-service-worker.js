if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('service-worker.js')
    .then(function() {
        console.log('service worker registered');
    }).catch(function(e) {
        console.error('failed while registring the service worker: ', e);
    });
}
